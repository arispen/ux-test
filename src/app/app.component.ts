import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  executions: string[] = [
    '8/2/21, 9:21 AM',
    '7/30/21, 7:19 AM',
    '7/30/21, 7:18 AM',
    '7/30/21, 7:17 AM',
    '7/30/21, 7:16 AM',
    '12/31/20, 23:59 PM',
  ];
  actions: string[] = [
    'Action 1 - echo',
    'Action 2 - sleep',
    'Action 3 - uname -a',
    'Action 4 - err',
  ];
  outputs: string[] = [
    `+ CommandLine - execute: COMMANDS: echo "test"  test`,
    `+ CommandLine - execute: COMMANDS: sleep 1`,
    `+ CommandLine - execute: COMMANDS: uname -a  Linux ip-172-31-13-116 5.4.0-1054-aws #57~18.04.1-Ubuntu SMP Thu Jul 15 03:21:36 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux`,
    `+ CommandLine - execute: COMMANDS: err


  
    Error :  exec error: Error: Command failed: err
    /bin/sh: 1: err: not found
    
     `
  ]
  getOutput(action: string) {
    const index = this.actions.indexOf(action);
    return this.outputs[index];
  }
}
